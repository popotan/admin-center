package cn.rock;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringCloudApplication
@EnableScheduling
@EnableFeignClients("cn.rock.api")
public class AdminCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminCenterApplication.class, args);
	}
}
